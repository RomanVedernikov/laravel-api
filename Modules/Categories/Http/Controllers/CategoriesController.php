<?php

namespace Modules\Categories\Http\Controllers;

use Modules\Categories\Http\Requests\CategoriesStoreRequest;
use Modules\Categories\Http\Resources\CategoriesResource;
use Modules\Categories\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CategoriesController extends Controller
{
    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoriesResource::collection(Categories::whereIsRoot()->get());
        //return CategoriesResource::collection(Categories::all()->toFlatTree());
        //return Categories::all()->toTree();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoriesStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesStoreRequest $request)
    {
        $category = Categories::create($request->all());

        return new CategoriesResource($category);
    }

    /**
     * Display the specified resource.
     *
     * @param Categories $category
     * @return CategoriesResource
     */
    public function show(Categories $category)
    {
        return new CategoriesResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoriesStoreRequest $request
     * @param Categories $category
     * @return void
     */
    public function update(CategoriesStoreRequest $request, Categories $category)
    {
        $category->update($request->all());

        return new CategoriesResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Categories $category
     * @return void
     */
    public function destroy(Categories $category)
    {
        $category->delete();
    }
}
