<?php

namespace Modules\Categories\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Categories\Models\Categories;

class CategoriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'parent_id' => $this->parent_id,
            'children' => CategoriesResource::collection($this->children()->get()),
        ];
    }
}
