<?php

namespace Modules\Products\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductsStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'image' => [
                'required',
                'image',
                'max:5120' // 5Mb
            ],
            'brand_id' => [
                'required',
                Rule::exists('brands', 'id'),
            ],
            'categories' => [
                'required',
                Rule::exists('categories', 'id'),
            ],
            'price' => 'required',
        ];
    }
}
