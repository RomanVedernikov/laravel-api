<?php

namespace Modules\Products\Http\Controllers;

use Modules\Products\Http\Requests\ProductsStoreRequest;
use Modules\Products\Http\Resources\ProductsResource;
use Modules\Products\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ProductsController extends Controller
{
    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ProductsResource::collection(Products::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductsStoreRequest  $request
     * @return ProductsResource
     */
    public function store(ProductsStoreRequest $request)
    {
        $product = Products::add($request->all());
        $product->uploadImage($request->file('image'));
        $product->setCategory($request->get('categories'));

        return new ProductsResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param Products $product
     * @return ProductsResource
     */
    public function show(Products $product)
    {
        return new ProductsResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductsStoreRequest $request
     * @param Products $product
     * @return ProductsResource
     */
    public function update(ProductsStoreRequest $request, Products $product)
    {
        $product->update($request->all());
        $product->uploadImage($request->file('image'));
        $product->setCategory($request->get('categories'));

        return new ProductsResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Products $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $product)
    {
        $product->remove();

        return response(null, \Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);
    }
}
