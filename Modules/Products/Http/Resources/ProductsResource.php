<?php

namespace Modules\Products\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\BrandsResource;
use App\Http\Resources\CategoriesResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'image' => $this->image,
            'price' => $this->price,
            'status' => $this->status,
            'short_description' => $this->short_description,
            'description' => $this->description,
            //'brand_id' => $this->brand_id,
            'brand' => new BrandsResource($this->brand),
            'categories' => CategoriesResource::collection($this->categories),
        ];
    }
}
