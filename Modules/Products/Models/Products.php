<?php

namespace Modules\Products\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use App\Models\Brands;
use App\Models\Categories;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory;
    use Sluggable;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'slug',
        'brand_id',
        'price',
        'status',
        'short_description',
        'description'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brands::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(
            Categories::class,
            'products_categories',
            'product_id',
            'category_id'
        );
    }

    /**
     * @param array $fields
     * @return Products
     */
    public static function add($fields = [])
    {
        $product = new self;
        $product->fill($fields);
        $product->save();

        return $product;
    }

    public function edit($fields = [])
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     *
     */
    public function remove()
    {
        $this->removeImage();
        $this->delete();
    }

    /**
     * @param $ids
     */
    public function setCategory($ids)
    {
        if ($ids == null) {
            return;
        }
        $this->categories()->sync($ids);
    }

    /**
     * @param $image
     */
    public function uploadImage($image)
    {
        if (is_null($image)) {
            return;
        }
        $this->removeImage();
        $this->image = $image->store('uploads', 'public');
        $this->save();
    }

    /**
     *
     */
    public function removeImage()
    {
        if (!is_null($this->image)) {
            Storage::delete($this->image);
        }
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
