<?php

namespace Modules\Users\Http\Controllers;

use Modules\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $token = $user->createToken('manager')->plainTextToken;

        return response([
            'user' => $user,
            'token' => $token
        ], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response([
                'message' => 'Credentials are incorrect'
            ], 401);
        }

        $token = $user->createToken('manager')->plainTextToken;

        return response([
            'user' => $user,
            'token' => $token
        ], 200);

    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return response([
            'message' => 'Succefully logout!'
        ]);
    }
}
