<?php

namespace Modules\Brands\Http\Controllers;

use Modules\Brands\Http\Requests\BrandsStoreRequest;
use Modules\Brands\Http\Resources\BrandsResource;
use Modules\Brands\Models\Brands;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class BrandsController extends Controller
{
    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return BrandsResource::collection(Brands::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandsStoreRequest $request
     * @return BrandsResource
     */
    public function store(BrandsStoreRequest $request)
    {
        $brand = Brands::add($request->all());
        $brand->uploadImage($request->file('image'));

        return new BrandsResource($brand);
    }

    /**
     * Display the specified resource.
     *
     * @param Brands $brand
     * @return BrandsResource
     */
    public function show(Brands $brand)
    {
        return new BrandsResource($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandsStoreRequest $request
     * @param Brands $brand
     * @return BrandsResource
     */
    public function update(BrandsStoreRequest $request, Brands $brand)
    {
        $brand->update($request->all());
        $brand->uploadImage($request->file('image'));

        return new BrandsResource($brand);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brands $brand
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(Brands $brand)
    {
        $brand->remove();

        return response(null, \Symfony\Component\HttpFoundation\Response::HTTP_NO_CONTENT);
    }
}
