<?php

namespace Modules\Brands\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brands extends Model
{
    use HasFactory;
    use Sluggable;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'slug',
        'description'
    ];

    /**
     * @param array $fields
     * @return Brands
     */
    public static function add($fields = [])
    {
        $brand = new self;
        $brand->fill($fields);
        $brand->save();

        return $brand;
    }

    /**
     *
     */
    public function remove()
    {
        $this->removeImage();
        $this->delete();

        return $this;
    }

    /**
     * @param $image
     * @return $this
     */
    public function uploadImage($image)
    {
        if (is_null($image)) {
            return $this;
        }
        $this->removeImage();
        $this->image = $image->store('uploads', 'public');
        $this->save();

        return $this;
    }

    /**
     *
     */
    public function removeImage()
    {
        if (!is_null($this->image)) {
            Storage::delete($this->image);
        }

        return $this;
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
